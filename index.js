var express = require("express");
var request = require("request");
var cheerio = require("cheerio");
var fs = require('fs');
var app = express();

var url = "http://nipponcolors.com";

var genHeader = function(option) {
  return  "// ==========================================================================\n" +
          "//\n" +
          "// Name:        " +(option.name||"")+ "\n" +
          "// Description: " +(option.description||"")+ "\n" +
          "// Version:     " +(option.version||"")+ "\n" +
          "//\n" +
          "// Author:      " +(option.author||"")+ "\n" +
          "// URL:         " +(option.url||"")+ "\n" +
          "//\n" +
          "// ==========================================================================\n\n";
};

var saveFile = function(path, list, header) {
  var content = header||"";
  list.forEach(function(e,i) {
    content += '@' +e.name+ ': ' +e.color+ ';\n';
  });
  fs.writeFile(path, content, function(err) {
    if (err) {
      return console.log(err);
    }
    console.log(path + " was successfully saved.");
  }); 
};

var fetch = function() {
  var list = [],
      style = {};
  // get color element id
  request(url, function(error, response, body) {
    var $ = cheerio.load(body);
    $('#colors li').each(function(i, e) {
      list.push({
        id: $(e).attr('id'),
        name: $(e).text().replace(/.*, /g,'').trim().toLowerCase()
      });
    });
    // match color id to color code
    request(url + "/min/g=nipponcolors_css", function(error, response, body) {
      style = body;
      list.forEach(function(e, i) {
        var reg = new RegExp('#' +e.id+ ' a:hover{background-color:#[A-Z0-9]{6}}', 'i');
        var rule = style.match(reg)[0];
        e.color = rule.match('#[A-Z0-9]{6}')[0];
        console.log('@' +e.name+ ': ' +e.color+ ';');
      });
      var header = genHeader({
        name: "Nippon Colors Palette",
        description: "The traditional colors of Japan.",
        version: "1.0.0",
        author: "Ono Takehiko",
        url: "http://nipponcolors.com/"
      });
      saveFile('./nipponcolors.less', list, header);
    });
  });
};

fetch();